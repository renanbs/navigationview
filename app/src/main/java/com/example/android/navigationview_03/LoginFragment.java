package com.example.android.navigationview_03;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.AccessToken;
import com.facebook.AccessTokenTracker;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Arrays;

import de.hdodenhof.circleimageview.CircleImageView;

public class LoginFragment extends Fragment {

	public static final String LOGIN_PREFERENCES = "LoginPreferences";

	onFacebookLogin mCallback;

	private Context mContext;
	private Activity mActivity;

	// Facebook login
	private CallbackManager callbackManager;
	private LoginButton facebookLoginButton;

	@Override
	public void onAttach(Context context) {
		super.onAttach(context);

		// This makes sure that the container activity has implemented
		// the callback interface. If not, it throws an exception
		if (context instanceof onFacebookLogin) {
			mCallback = (onFacebookLogin) context;
		} else {
			throw new ClassCastException(context.toString()
					+ " must implement onFacebookLogin");
		}
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		mActivity = getActivity();
		mContext = getActivity().getApplicationContext();
		FacebookSdk.sdkInitialize(mContext);

		AccessTokenTracker accessTokenTracker = new AccessTokenTracker() {
			@Override
			protected void onCurrentAccessTokenChanged(AccessToken oldAccessToken, AccessToken newAccessToken) {
				updateWithToken(newAccessToken);
			}
		};

	}

	private void updateWithToken(AccessToken currentAccessToken) {

		if (currentAccessToken != null) {
			Log.e("LOGGED", "Logged");
		} else {
			Log.e("LOGGED", "NOT Logged");
		}
	}

	/*
	Register a callback function with LoginButton to respond to the login result.
    On successful login,login result has new access token and  recently granted permissions.
    */
	protected void getLoginDetails() {

		// Callback registration
		facebookLoginButton.registerCallback(callbackManager, new FacebookCallback<LoginResult>() {
			@Override
			public void onSuccess(LoginResult login_result) {
				getUserInfo(login_result);
				onBackPressed();
			}

			@Override
			public void onCancel() {
				Toast.makeText(getActivity(), "User cancelled", Toast.LENGTH_SHORT).show();
			}

			@Override
			public void onError(FacebookException exception) {
				Toast.makeText(getActivity(), "Error on Login, check your facebook app_id", Toast.LENGTH_LONG).show();
			}
		});
	}

	public void onBackPressed() {
		FragmentManager fm = getActivity().getSupportFragmentManager();
		fm.popBackStack();
	}

	/*
		To get the facebook user's own profile information via  creating a new request.
		When the request is completed, a callback is called to handle the success condition.
	 */
	protected void getUserInfo(LoginResult login_result) {
		GraphRequest data_request = GraphRequest.newMeRequest(
				login_result.getAccessToken(),
				new GraphRequest.GraphJSONObjectCallback() {
					@Override
					public void onCompleted(
							JSONObject json_object,
							GraphResponse response) {
						SharedPreferences settings = mActivity.getSharedPreferences(LOGIN_PREFERENCES, 0);
						SharedPreferences.Editor editor = settings.edit();


						try {
							if (json_object.has("name")) {
								String name = json_object.getString("name");
								editor.putString("name", name);
								mCallback.setProfileName(name);
							}
						} catch (JSONException e) {
							e.printStackTrace();
						}
						try {
							String email = json_object.getString("email");
							editor.putString("email", email);
							mCallback.setEmail(email);
						} catch (JSONException e) {
							e.printStackTrace();
						}
						try {
							if (json_object.has("picture")) {
								String profilePicUrl = json_object.getJSONObject("picture").getJSONObject("data").getString("url");
								mCallback.setProfilePicture(profilePicUrl);
							}
						} catch (Exception e) {
							e.printStackTrace();
						}

						// Commit the edits!
						editor.commit();
					}
				});
		Bundle permission_param = new Bundle();
		permission_param.putString("fields", "id,name,email,picture.width(64).height(64)");
		data_request.setParameters(permission_param);
		data_request.executeAsync();
	}

	public void configureFacebookLogin(View view) {
		//Facebook Login
		facebookLoginButton = (LoginButton) view.findViewById(R.id.f_sign_in_button);
		facebookLoginButton.setReadPermissions(Arrays.asList("public_profile, email, user_birthday, user_friends"));
		facebookLoginButton.setFragment(this);

		callbackManager = CallbackManager.Factory.create();

		getLoginDetails();
	}

	public void logoffFace() {
		AccessToken accessToken = AccessToken.getCurrentAccessToken();
		if (accessToken != null) {
			LoginManager.getInstance().logOut();
			TextView em = (TextView) getActivity().findViewById(R.id.email);
			em.setText("Olá visitante!");

			CircleImageView img = (CircleImageView) getActivity().findViewById(R.id.profile_image);

			Drawable drw = ContextCompat.getDrawable(mContext, R.drawable.background_material);
			img.setImageDrawable(drw);
		}
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
							 Bundle savedInstanceState) {
		// Inflate the layout for this fragment
		View view = inflater.inflate(R.layout.fragment_login, container, false);

		configureFacebookLogin(view);

		Button button = (Button) view.findViewById(R.id.logout);
		button.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				logoffFace();
			}
		});

		return view;
	}

	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		callbackManager.onActivityResult(requestCode, resultCode, data);
	}

	public interface onFacebookLogin {
		void setEmail(String email);

		void setProfileName(String name);

		void setProfilePicture(String url);
	}
}
