package com.example.android.navigationview_03;

import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.ImageRequest;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by Admin on 04-06-2015.
 */
public class SentFragment extends Fragment {

	TextView mTextView;
	ImageView mImageView;

	TextView mTextView_name;
	TextView mTextView_codename;
	@Nullable
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		View v = inflater.inflate(R.layout.sent_fragment,container,false);

		Button button = (Button) v.findViewById(R.id.button_send);
		button.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				send();
			}
		});

		Button buttonImage = (Button) v.findViewById(R.id.button_image);
		buttonImage.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				getImage();
			}
		});

		Button buttonJSON = (Button) v.findViewById(R.id.button_json);
		buttonJSON.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				getJSON();
			}
		});

		mTextView = (TextView) v.findViewById(R.id.textView);
		mImageView = (ImageView) v.findViewById(R.id.myImage);
		mTextView_name = (TextView) v.findViewById(R.id.textView_name);
		mTextView_codename = (TextView) v.findViewById(R.id.textView_codename);
		return v;
	}

	public void send()
	{

		// Instantiate the RequestQueue.
		RequestQueue queue = Volley.newRequestQueue(getContext());
		String url ="http://web02-vikingspoa.rhcloud.com/AccessDb";

	// Request a string response from the provided URL.
		StringRequest stringRequest = new StringRequest(Request.Method.GET, url,
				new Response.Listener<String>() {
					@Override
					public void onResponse(String response) {
						// Display the first 500 characters of the response string.
						mTextView.setText("Response is: "+ response);
					}
				}, new Response.ErrorListener() {
			@Override
			public void onErrorResponse(VolleyError error) {
				mTextView.setText("That didn't work!");
			}
		});
	// Add the request to the RequestQueue.
		queue.add(stringRequest);
	}

	public void getImage()
	{

		String url = "http://i.imgur.com/7spzG.png";



// Retrieves an image specified by the URL, displays it in the UI.
		ImageRequest request = new ImageRequest(url,
				new Response.Listener<Bitmap>() {
					@Override
					public void onResponse(Bitmap bitmap) {
						mImageView.setImageBitmap(bitmap);
					}
				}, 0, 0, null,
				new Response.ErrorListener() {
					public void onErrorResponse(VolleyError error) {
						Log.e("erro", "error");
					}
				});
// Access the RequestQueue through your singleton class.
		MySingleton.getInstance(getContext()).addToRequestQueue(request);
	}

	public void getJSON(){

		String url = "http://web02-vikingspoa.rhcloud.com/AccessDb";

		JsonObjectRequest jsObjRequest = new JsonObjectRequest
				(Request.Method.GET, url, null, new Response.Listener<JSONObject>() {

					@Override
					public void onResponse(JSONObject response) {
						try {
							String name = response.getString("nome");
							mTextView_name.setText(name);

							String codename = response.getString("codinome");
							mTextView_codename.setText(codename);
						} catch (JSONException e) {
							e.printStackTrace();
						}

					}
				}, new Response.ErrorListener() {

					@Override
					public void onErrorResponse(VolleyError error) {
						// TODO Auto-generated method stub
						Log.e("erro", "error");

					}
				});

// Access the RequestQueue through your singleton class.
		MySingleton.getInstance(getContext()).addToRequestQueue(jsObjRequest);
	}
}