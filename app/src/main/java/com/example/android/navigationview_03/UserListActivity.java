package com.example.android.navigationview_03;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Toast;

import java.util.ArrayList;

/**
 * Created by renan on 4/18/16.
 */
public class UserListActivity extends AppCompatActivity {

	ArrayList<Contact> contacts;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_users);
		// ...
		// Lookup the recyclerview in activity layout
		RecyclerView rvContacts = (RecyclerView) findViewById(R.id.rvContacts);

		// Initialize contacts
		contacts = Contact.createContactsList(20);
		// Create adapter passing in the sample user data
		ContactsAdapter adapter = new ContactsAdapter(contacts);
		adapter.setOnItemClickListener(new ContactsAdapter.OnItemClickListener() {
			@Override
			public void onItemClick(View view, int position) {
				String name = contacts.get(position).getName();
				Toast.makeText(UserListActivity.this, name + " was clicked!", Toast.LENGTH_SHORT).show();
				Intent intent = new Intent(UserListActivity.this, Second.class);
				startActivity(intent);
			}
		});
		// Attach the adapter to the recyclerview to populate items
		rvContacts.setAdapter(adapter);
		// Set layout manager to position the items
		rvContacts.setLayoutManager(new LinearLayoutManager(this));
		// That's all!
	}
}

