package com.example.android.navigationview_03;

import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.design.widget.NavigationView;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.AccessToken;
import com.facebook.AccessTokenTracker;
import com.facebook.FacebookSdk;
import com.facebook.login.LoginManager;
import com.squareup.picasso.Picasso;

import de.hdodenhof.circleimageview.CircleImageView;


public class MainActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener, LoginFragment.onFacebookLogin{

	public static final String LOGIN_PREFERENCES = "LoginPreferences";

	private static final String SELECTED_ITEM_ID = "selected";
	private static final String FIRST_TIME = "first_time";
	private Toolbar mToolbar;
	private NavigationView mDrawer;
	private DrawerLayout mDrawerLayout;
	private ActionBarDrawerToggle mDrawerToggle;

	private View headerView = null;

	private boolean isLogged = false;
	private int mSelectedId;
	private boolean mUserSawDrawer = false;

	LoginFragment login_fragment;

	@Override
	public void setEmail(String email) {
		TextView em = (TextView)findViewById(R.id.email);
		em.setText(email);
	}

	@Override
	public void setProfileName(String name) {
		TextView n = (TextView)findViewById(R.id.name);
		n.setText(name);
	}

	@Override
	public void setProfilePicture (String url) {
		CircleImageView img = (CircleImageView) findViewById(R.id.profile_image);
		Picasso.with(this).load(url).into(img);
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);

		mToolbar = (Toolbar) findViewById(R.id.app_bar);
		setSupportActionBar(mToolbar);

		mDrawer = (NavigationView) findViewById(R.id.main_drawer);
		mDrawer.setNavigationItemSelectedListener(this);
		mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);

		mDrawerToggle = new ActionBarDrawerToggle(this, mDrawerLayout, mToolbar, R.string.drawer_open, R.string.drawer_close);
		mDrawerLayout.addDrawerListener(mDrawerToggle);

		// ADD the header by hand
		headerView = getLayoutInflater().inflate(R.layout.drawer_header, mDrawer, false);
		mDrawer.addHeaderView(headerView);

		// Make the rounded button click work
		ImageView headerImage = (ImageView) headerView.findViewById(R.id.profile_image);
		headerImage.setOnClickListener(new View.OnClickListener() {
			public void onClick(View view) {
				login(view);
			}
		});

		mDrawerToggle.syncState();

		if(!didUserSeeDrawer())
		{
			showDrawer();
			markDrawerSeen();
		}
		else {
			hideDrawer();
		}
		mSelectedId = savedInstanceState == null ? R.id.sent_mail : savedInstanceState.getInt(SELECTED_ITEM_ID);
		navigate(mSelectedId);

		// Get access to the detail view fragment by id
		login_fragment = (LoginFragment) getSupportFragmentManager()
				.findFragmentById(R.id.login_fragment_face);

		FacebookSdk.sdkInitialize(getApplicationContext());

		AccessTokenTracker accessTokenTracker = new AccessTokenTracker() {
			@Override
			protected void onCurrentAccessTokenChanged(AccessToken oldAccessToken, AccessToken newAccessToken) {
				if (newAccessToken != null) {
					Log.e("LOGGED", "Logged");
					isLogged = true;
					updateHeaderProfile();
				} else {
					Log.e("LOGGED", "NOT Logged");
					isLogged = false;
//					login_fragment = new LoginFragment();
//					android.support.v4.app.FragmentTransaction loginFragment = getSupportFragmentManager().beginTransaction();
//					loginFragment.replace(R.id.frame, login_fragment);
//					loginFragment.addToBackStack(null);
//					loginFragment.commit();
				}
			}
		};
	}

	void updateHeaderProfile()
	{
		SharedPreferences settings = getSharedPreferences(LOGIN_PREFERENCES, 0);

		String name = settings.getString("name", "false");
		TextView n = (TextView)headerView.findViewById(R.id.name);
		n.setText(name);

		String email = settings.getString("email", "false");
		TextView em = (TextView)headerView.findViewById(R.id.email);
		em.setText(email);

		Menu menuNav = mDrawer.getMenu();
		MenuItem logoutItem = menuNav.findItem(R.id.logout_menu);
		logoutItem.setVisible(isLogged);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.menu_main, menu);
		return true;
	}

	@Override
	public boolean onPrepareOptionsMenu(Menu menu) {
		super.onPrepareOptionsMenu(menu);

		return true;
	}

	private boolean didUserSeeDrawer()
	{
		SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
		mUserSawDrawer = sharedPreferences.getBoolean(FIRST_TIME, false);
		return mUserSawDrawer;
	}

	private void markDrawerSeen()
	{
		SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
		mUserSawDrawer = true;
		sharedPreferences.edit().putBoolean(FIRST_TIME, mUserSawDrawer).apply();
	}
	private void showDrawer()
	{
		mDrawerLayout.openDrawer(GravityCompat.START);
	}
	private void hideDrawer()
	{
		mDrawerLayout.closeDrawer(GravityCompat.START);
	}

	public void logoffFace() {
		AccessToken accessToken = AccessToken.getCurrentAccessToken();
		if (accessToken != null) {
			LoginManager.getInstance().logOut();
			TextView em = (TextView) findViewById(R.id.email);
			em.setText("Olá visitante!");

			CircleImageView img = (CircleImageView) findViewById(R.id.profile_image);

			Drawable drw = ContextCompat.getDrawable(this, R.drawable.background_material);
			img.setImageDrawable(drw);

			TextView name = (TextView) findViewById(R.id.name);
			name.setText("");
			isLogged = false;
		}
		Menu menuNav = mDrawer.getMenu();
		MenuItem logoutItem = menuNav.findItem(R.id.logout_menu);
		logoutItem.setVisible(isLogged);

	}

	private void navigate(int selectedId)
	{
		switch (selectedId)
		{
			case R.id.second:
				mDrawerLayout.closeDrawer(GravityCompat.START);
				Intent intent = new Intent(this, UserListActivity.class);
				startActivity(intent);
				return;

			case R.id.logout_menu:
				mDrawerLayout.closeDrawer(GravityCompat.START);
				logoffFace();
				return;
			case R.id.allmail:

				mDrawerLayout.closeDrawer(GravityCompat.START);
				ContentFragment content = new ContentFragment();
				android.support.v4.app.FragmentTransaction contentTransaction = getSupportFragmentManager().beginTransaction();
				contentTransaction.replace(R.id.frame, content);
				contentTransaction.addToBackStack("ALL_MAIL");
				contentTransaction.commit();
				return;

			case R.id.drafts:
				mDrawerLayout.closeDrawer(GravityCompat.START);
				DraftFragment fragment = new DraftFragment();
				android.support.v4.app.FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
				fragmentTransaction.replace(R.id.frame, fragment);
				fragmentTransaction.addToBackStack("Drafts");
				fragmentTransaction.commit();
				return;

			case R.id.spam:
				mDrawerLayout.closeDrawer(GravityCompat.START);
				SpamFragment spamF = new SpamFragment();
				android.support.v4.app.FragmentTransaction spamFragment = getSupportFragmentManager().beginTransaction();
				spamFragment.replace(R.id.frame, spamF);
				spamFragment.addToBackStack("SPAM");
				spamFragment.commit();
				return;

			case R.id.trash:
				mDrawerLayout.closeDrawer(GravityCompat.START);
				TrashFragment trashF = new TrashFragment();
				android.support.v4.app.FragmentTransaction trashFragment = getSupportFragmentManager().beginTransaction();
				trashFragment.replace(R.id.frame, trashF);
				trashFragment.addToBackStack("TRASH");
				trashFragment.commit();
				return;

			case R.id.sent_mail:
				mDrawerLayout.closeDrawer(GravityCompat.START);
				SentFragment sentF = new SentFragment();
				android.support.v4.app.FragmentTransaction sentFragment = getSupportFragmentManager().beginTransaction();
				sentFragment.replace(R.id.frame, sentF);
				sentFragment.addToBackStack("SENT");
				sentFragment.commit();
				return;

			default:
				Toast.makeText(MainActivity.this, "default", Toast.LENGTH_SHORT).show();
		}
	}

	@Override
	public void onConfigurationChanged(Configuration newConfig)
	{
		super.onConfigurationChanged(newConfig);
		mDrawerToggle.onConfigurationChanged(newConfig);
	}

	@Override
	public boolean onNavigationItemSelected(MenuItem menuItem)
	{
		menuItem.setChecked(true);

		mSelectedId = menuItem.getItemId();
		navigate(mSelectedId);
		return true;
	}

	@Override
	protected void onSaveInstanceState(Bundle bundle)
	{
		super.onSaveInstanceState(bundle);
		bundle.putInt(SELECTED_ITEM_ID, mSelectedId);
	}

	@Override
	public void onBackPressed() {
		if (mDrawerLayout.isDrawerOpen(GravityCompat.START))
		{
			mDrawerLayout.closeDrawer(GravityCompat.START);
		}
		else
		{
			super.onBackPressed();
		}
	}

	public void login(View view) {
//		mDrawerLayout.closeDrawer(GravityCompat.START);

		// SignUp/SignIn/Profile
//		startActivity(new Intent(view.getContext(), LoginActivity.class));
		if (!isLogged) {
			mDrawerLayout.closeDrawer(GravityCompat.START);
			login_fragment = new LoginFragment();
			android.support.v4.app.FragmentTransaction loginFragment = getSupportFragmentManager().beginTransaction();
			loginFragment.replace(R.id.frame, login_fragment);
			loginFragment.addToBackStack(null);
			loginFragment.commit();
		}
	}
}
