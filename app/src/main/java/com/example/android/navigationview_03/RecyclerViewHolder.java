package com.example.android.navigationview_03;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

/**
 * Created by kundan on 10/26/2015.
 */
public class RecyclerViewHolder extends RecyclerView.ViewHolder
        implements View.OnClickListener {
    TextView tv1,tv2;
    ImageView imageView;
//    public IMyViewHolderClicks mListener;

    public RecyclerViewHolder(View itemView) {
        super(itemView);

        tv1= (TextView) itemView.findViewById(R.id.list_title);
        tv1.setOnClickListener(this);
        tv2= (TextView) itemView.findViewById(R.id.list_desc);
        tv2.setOnClickListener(this);
        imageView= (ImageView) itemView.findViewById(R.id.list_avatar);
        imageView.setOnClickListener(this);
    }
    @Override
    public void onClick(View view) {
        Toast.makeText(view.getContext(), "Position: " + getLayoutPosition(), Toast.LENGTH_SHORT).show();
    }

//    public static interface IMyViewHolderClicks {
//        public void onTextViewClick(TextView caller);
//        public void onImageViewClic(ImageView callerImage);
//    }
}

