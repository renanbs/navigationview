package com.example.android.navigationview_03;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;

public class TabFragment1 extends Fragment {

	private static RecyclerView.Adapter adapter;
	private RecyclerView.LayoutManager layoutManager;
	private static RecyclerView recyclerView;
	private static ArrayList<DataModel> data;
//	static View.OnClickListener myOnClickListener;
	private static ArrayList<Integer> removedItems;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		View v = inflater.inflate(R.layout.tab_fragment_1, container, false);

		recyclerView = (RecyclerView) v.findViewById(R.id.my_recycler_view);
		recyclerView.setHasFixedSize(true);

		layoutManager = new LinearLayoutManager(getContext());
		recyclerView.setLayoutManager(layoutManager);
		recyclerView.setItemAnimator(new DefaultItemAnimator());

		data = new ArrayList<DataModel>();
		for (int i = 0; i < MyData.nameArray.length; i++) {
			data.add(new DataModel(
					MyData.nameArray[i],
					MyData.versionArray[i],
					MyData.id_[i],
					MyData.drawableArray[i]
			));
		}

		removedItems = new ArrayList<Integer>();

		adapter = new CustomAdapter(data);
		recyclerView.setAdapter(adapter);
		recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));

		return v;
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
//		adapter = new CustomAdapter(data);
//		recyclerView.setAdapter(adapter);
	}
}
